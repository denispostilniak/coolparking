﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase {
        private IParkingService parkingService;

        public ParkingController(IParkingService service) {
            parkingService = service;
        }

        [HttpGet("balance")]
        public ActionResult<decimal> GetBalance() {
            return Ok( parkingService.GetBalance());
        }

        [HttpGet("capacity")]
        public ActionResult<int> GetCapacity() {
            return Ok(parkingService.GetCapacity());
        }

        [HttpGet("freePlaces")]
        public ActionResult<int> GetFreePlaces() {
            return Ok(parkingService.GetFreePlaces());
        }
    }
}