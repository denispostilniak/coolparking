﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.WebAPI.Models;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using AutoMapper;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase {
        private IParkingService parkingService;

        public VehiclesController(IParkingService service) {
            parkingService = service;
        }

        [HttpGet()]
        public ActionResult GetVehicles() {
            try {
                var vehicles = parkingService.GetVehicles();

                return Ok(vehicles);
            } catch(Exception e) {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("{id}")]
        public ActionResult GetVehiclesById(string id) {
            if(id == null) {
                return NotFound();
            }

            if (!IsIdValid(id)) {
                return BadRequest();
            }

            var vehicles = parkingService.GetVehicles();
            var vehiclesById = vehicles.Where(v => v.Id == id).FirstOrDefault();

            if(vehiclesById == null) {
                return NotFound();
            }
            
            var vehiclesInJson = new JsonResult(vehiclesById);
            return Ok(vehiclesInJson);
        }

        [HttpPost()]
        public ActionResult AddVehicle([FromBody]VehicleModel vehicle) {
            try {
                var config = new MapperConfiguration(cfg => cfg.CreateMap<VehicleModel, Vehicle>());
                var mapper = new Mapper(config);
                Vehicle model = mapper.Map<VehicleModel, Vehicle>(vehicle);

                parkingService.AddVehicle(model);

                return CreatedAtAction(nameof(GetVehiclesById), new { id = model.Id }, model);

            } catch {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public ActionResult RemoveVehicle(string id) {

            try {
                parkingService.RemoveVehicle(id);

                return NoContent();
            } catch (InvalidOperationException e) {
                return BadRequest(e.Message);
            } catch (ArgumentException e) {
                return NotFound(e.Message);
            }
        }

        public bool IsIdValid(string id) {
            string regularExpression = "[A-Z][A-Z][-][0-9]{4}[-][A-Z][A-Z]";

            return (Regex.IsMatch(id, regularExpression));
        }
    }
}