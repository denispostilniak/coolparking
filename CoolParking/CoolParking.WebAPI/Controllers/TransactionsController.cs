﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Services;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using AutoMapper;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private IParkingService parkingService;
        private IMapper mapper;
        public TransactionsController(IParkingService service, IMapper map) {
            parkingService = service;
            mapper = map;
        }
        [HttpGet("last")] 
       public ActionResult GetLastTransactions() {
            try {
                var model = parkingService.GetLastParkingTransactions()
                                          .Select(m => mapper.Map<TransactionModel>(m));

                return Ok(model);
            } catch(Exception e) {

                return BadRequest(e.Message);
            }
           
       }

        [HttpGet("all")]
        public ActionResult GetAllTransactions() {
            try {

                return Ok(parkingService.ReadFromLog());
            } catch (InvalidOperationException e) {

                return NotFound(e.Message);
            } catch (Exception e) {

                return BadRequest(e.Message);
            }

        }

        [HttpPut("topUpVehicle")]
        public ActionResult TopUpVehicle([FromBody]TopUpVehicleModel model) {
            try {
                parkingService.TopUpVehicle(model.Id, model.Sum);

                var result = new JsonResult(parkingService.GetVehicles().Where(t => t.Id == model.Id).FirstOrDefault());

                return Ok(result);

            } catch(InvalidOperationException e) {

                return NotFound(e.Message);
            } catch(Exception e) {

                return BadRequest(e.Message);
            }
        }
    }
}