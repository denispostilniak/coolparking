﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Models
{
    public class TopUpVehicleModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("Sum")]
        public decimal Sum { get; set; }
    }
}
