﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Models
{
    public class TransactionModel
    {
        [JsonProperty("vehicleId")]
        public string VehycleId { get; set; }
        [JsonProperty("sum")]
        public decimal Sum { get; set; }
        [JsonProperty("transactionDate")]
        public DateTime TransactionDate { get; set; }
    }
}
