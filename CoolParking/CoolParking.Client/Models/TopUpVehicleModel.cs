﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace CoolParking.Client.Models
{
   public class TopUpVehicleModel {

        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("Sum")]
        public decimal Sum { get; set; }
    }
}
