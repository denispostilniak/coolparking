﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace CoolParking.Client.Models
{
    public class TransactionModel
    {
        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }
        [JsonProperty("sum")]
        public decimal Sum { get; set; }
        [JsonProperty("transactionDate")]
        public DateTime TransactionDate { get; set; }
    }
}
