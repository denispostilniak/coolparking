﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.Client.Settings
{
    public static class HttpSettings
    {
        public static string Host { get; } = "http://localhost:51383/";
        public static string ApiEndpoint { get; } = "api/";

        public static string VehicleEndpoint { get; } = "vehicles/";

        public static string TransactionEndpoint { get; } = "transactions/";
        public static string TransactionEndpointLast { get; } = "last";
        public static string TransactionEndpointAll { get; } = "all";
        public static string TransactionEndpointTopUpVehicle { get; } = "topUpVehicle";

        public static string ParkingEndpoint { get; } = "parking/";
        public static string ParkingEndpointBalance { get; } = "balance";
        public static string ParkingEndpointCapacity { get; } = "capacity";
        public static string ParkingEndpointFreePlaces { get; } = "freePlaces";
    }
}
