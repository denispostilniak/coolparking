﻿using System;
using CoolParking.Client.Interfaces;
using CoolParking.Client.Services;

namespace CoolParking.BL
{
    public class Program
    {
        public static async void Main(string[] args) {
            string option;
            int parseOption = 0;
            int maxOptionValue = 10;
            int minOptionValue = 1;

            var consoleService = new ConsoleService(new ClientService());

            while (true) {
                Console.WriteLine("Виберіть опцію, яку хочете використати: \n");

                Console.WriteLine("1: Вивести на екран поточний баланс Паркінгу \n");

                Console.WriteLine("2: Вивести на екран суму зароблених коштів за поточний період \n");

                Console.WriteLine("3: Вивести на екран кількість вільних місць на паркуванні \n");

                Console.WriteLine("4: Вивести на екран усі Транзакції Паркінгу за поточний період \n");

                Console.WriteLine("5: Вивести на екран історію Транзакцій \n");

                Console.WriteLine("6: Вивести на екран список Тр. засобів , що знаходяться на Паркінгу \n");

                Console.WriteLine("7: Поставити Транспортний засіб на Паркінг \n");

                Console.WriteLine("8: Забрати Транспортний засіб з Паркінгу \n");

                Console.WriteLine("9: Поповнити баланс конкретного Тр. засобу \n");

                Console.WriteLine("10: Вихід\n");

                Console.WriteLine("Ваш вибір: ");
                option = Console.ReadLine();
                try {
                    parseOption = Int32.Parse(option);
                } catch {
                    Console.WriteLine("Невірний вхідний формат! Спробуйте ще");
                    continue;
                }
                if (parseOption >= minOptionValue && parseOption <= maxOptionValue) {
                    switch (parseOption) {
                        case 1:
                           await consoleService.ShowCurrentParkingBalance();
                            break;
                        case 2:
                           await consoleService.ShowEarnsMoneyForPeriod();
                            break;
                        case 3:
                           await consoleService.ShowFreePlaces();
                            break;
                        case 4:
                           await consoleService.ShowAllTransactionForPeriod();
                            break;
                        case 5:
                           await consoleService.ShowTransactionHistory();
                            break;
                        case 6:
                           await consoleService.ShowVehicles();
                            break;
                        case 7:
                           await consoleService.AddVehicleOnParking();
                            break;
                        case 8:
                           await consoleService.RemoveVehicleFromParking();
                            break;
                        case 9:
                           await consoleService.TopUpVehicleBalance();
                            break;
                        case 10:
                            return;
                    }

                } else {
                    Console.WriteLine("Значення повинні бути між 1 та 10");
                    continue;
                }
            }

        }
    }
}