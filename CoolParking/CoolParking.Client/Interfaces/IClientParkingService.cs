﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Client.Interfaces
{
    public interface IClientParkingService
    {
        Task<double> GetParkignBalance();
        Task<int> GetParkingCapacity();
        Task<int> GetParkingFreePlaces();
    }
}
