﻿using System;
using System.Collections.Generic;
using System.Text;
using CoolParking.Client.Interfaces;

namespace CoolParking.Client.Interfaces
{
    public interface IClientService : IClientParkingService,
                                      IClientTransactionService,
                                      IClientVehicleService
    {
    }
}
