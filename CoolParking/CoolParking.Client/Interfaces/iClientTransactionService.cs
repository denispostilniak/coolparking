﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CoolParking.Client.Models;

namespace CoolParking.Client.Interfaces
{
    public interface IClientTransactionService
    {
        Task<IEnumerable<TransactionModel>> GetLastParkingTransactions();
        Task<string> GetAllTrancsations();
        Task<VehicleModel> TopUpVehicle(TopUpVehicleModel model);
    }
}
