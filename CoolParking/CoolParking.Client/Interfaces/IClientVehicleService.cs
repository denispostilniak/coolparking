﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CoolParking.Client.Models;

namespace CoolParking.Client.Interfaces
{
    public interface IClientVehicleService
    {
        Task<IEnumerable<VehicleModel>> GetVehicles();
        Task<VehicleModel> GetVehicle(string Id);
        Task AddVehicle(VehicleModel model);
        Task RemoveVehicle(string vehicleId);
    }
}
