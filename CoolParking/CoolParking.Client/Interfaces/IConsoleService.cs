﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Client.Interfaces
{
    interface IConsoleService
    {
        Task ShowCurrentParkingBalance();
        Task ShowEarnsMoneyForPeriod();
        Task ShowFreePlaces();
        Task ShowAllTransactionForPeriod();
        Task ShowTransactionHistory();
        Task ShowVehicles();
        Task<bool> AddVehicleOnParking();
        Task<bool> RemoveVehicleFromParking();
        Task<bool> TopUpVehicleBalance();
    }
}
