﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CoolParking.Client.Interfaces;
using CoolParking.Client.Models;
using CoolParking.Client.Settings;
using Newtonsoft.Json;

namespace CoolParking.Client.Services
{
    public class ClientService : IClientService
    {
        private readonly HttpClient _client;
        public ClientService() {
            _client = new HttpClient { BaseAddress = new Uri(HttpSettings.Host + HttpSettings.ApiEndpoint) };
        }
        async Task ResponseStatus(HttpResponseMessage response) {
            if (response.IsSuccessStatusCode) {
                return;
            }

            var messageIfNotSuccess = await response.Content.ReadAsStringAsync();

            throw new Exception(response.StatusCode + messageIfNotSuccess);
        }
        public async Task AddVehicle(VehicleModel model) {
            string Url = HttpSettings.VehicleEndpoint;

            var inputJson = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            var response = await _client.PostAsync(Url, inputJson);
            await ResponseStatus(response);
        }
        public async Task RemoveVehicle(string vehicleId) {
            string Url = HttpSettings.VehicleEndpoint + vehicleId;

            var response = await _client.DeleteAsync(Url);
            await ResponseStatus(response);
        }

        public async Task<int> GetParkingFreePlaces() {
            string Url = HttpSettings.ParkingEndpoint + HttpSettings.ParkingEndpointFreePlaces;

            var response = await _client.GetAsync(Url);
            await ResponseStatus(response);
            var content = response.Content;

            try {
                int freePlaces = JsonConvert.DeserializeObject<int>(await content.ReadAsStringAsync());

                return freePlaces;
            } catch (Exception e) {

                Console.WriteLine(e.Message);
            }

            return 0;
        }

        public async Task<IEnumerable<VehicleModel>> GetVehicles() {
            string Url = HttpSettings.VehicleEndpoint;

            var response = await _client.GetAsync(Url);
            await ResponseStatus(response);
            var content = response.Content;

            try {
                var vehicles = JsonConvert.DeserializeObject<IEnumerable<VehicleModel>>(await content.ReadAsStringAsync());

                return vehicles;
            } catch (Exception e) {

                Console.WriteLine(e.Message);
            }

            return null;
        }
        public async Task<VehicleModel> GetVehicle(string vehicleId) {
            string Url = HttpSettings.VehicleEndpoint + vehicleId;

            var response = await _client.GetAsync(Url);
            await ResponseStatus(response);
            var content = response.Content;

            try {
                VehicleModel vehicle = JsonConvert.DeserializeObject<VehicleModel>(await content.ReadAsStringAsync());

                return vehicle;
            } catch (Exception e) {

                Console.WriteLine(e.Message);
            }

            return null;
        }
       

        public async Task<IEnumerable<TransactionModel>> GetLastParkingTransactions() {
            string Url = HttpSettings.TransactionEndpoint + HttpSettings.TransactionEndpointLast;

            var response = await _client.GetAsync(Url);
            await ResponseStatus(response);
            var content = response.Content;

            try {
                TransactionModel[] transactions = JsonConvert.DeserializeObject<TransactionModel[]>(await content.ReadAsStringAsync());
                
                return transactions;
            } catch (Exception e) {

                Console.WriteLine(e.Message);
            }

            return null;
        }
        public async Task<double> GetParkignBalance() {
            string Url = HttpSettings.ParkingEndpoint + HttpSettings.ParkingEndpointBalance;

            var response = await _client.GetAsync(Url);

            await ResponseStatus(response);
            var content = response.Content;

            try {
                double balance = JsonConvert.DeserializeObject<double>(await content.ReadAsStringAsync());

                return balance;
            } catch (Exception e) {

                Console.WriteLine(e.Message);
            }

            return 0;
        }
        public async Task<int> GetParkingCapacity() {
            string Url = HttpSettings.ParkingEndpoint + HttpSettings.ParkingEndpointCapacity;

            var response = await _client.GetAsync(Url);

            await ResponseStatus(response);
            var content = response.Content;

            try {
                int parkingCapacity = JsonConvert.DeserializeObject<int>(await content.ReadAsStringAsync());

                return parkingCapacity;
            } catch (Exception e) {

                Console.WriteLine(e.Message);
            }

            return 0;
        }
        public async Task<string> GetAllTrancsations() {
            string Url = HttpSettings.TransactionEndpoint + HttpSettings.TransactionEndpointAll;

            var response = await _client.GetAsync(Url);
            await ResponseStatus(response);
            var content = response.Content;

            try {
                string transactions = await content.ReadAsStringAsync();

                return transactions;
            } catch (Exception e) {

                Console.WriteLine(e.Message);
            }

            return null;
        }
        public async Task<VehicleModel> TopUpVehicle(TopUpVehicleModel model) {
            string Url = HttpSettings.TransactionEndpoint + HttpSettings.TransactionEndpointTopUpVehicle;
            var inputJson = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            var response = await _client.PutAsync(Url, inputJson);
            await ResponseStatus(response);
            var responseContent = response.Content;

            try {
                VehicleModel vehicle = JsonConvert.DeserializeObject<VehicleModel>(await responseContent.ReadAsStringAsync());

                return vehicle;
            } catch (Exception e) {

                Console.WriteLine(e.Message);
            }

            return null;
        }
    }
}
