﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CoolParking.Client.Interfaces;
using CoolParking.BL.Models;
using AutoMapper;
using CoolParking.Client.Models;
using System.Linq;

namespace CoolParking.Client.Services
{
    public class ConsoleService : IConsoleService {
        private readonly IClientService _clientService;
        public ConsoleService(IClientService clientService) {
            _clientService = clientService;
        }
        public async Task<bool> RemoveVehicleFromParking() {
            Console.WriteLine("Введіть ID ТЗ, якого хочете вилучити: ");

            string vehicleId = Console.ReadLine();

            if (!isIdValid(vehicleId)) {
                Console.WriteLine("Id повинно бути типу AA-1111-AA. Спробуйте ще раз!");

                return false;
            }

            var vehicle = await _clientService.GetVehicles();
            if (vehicle == null) {
                Console.WriteLine("Даний ТЗ не існує");
                return false;
            }
            await _clientService.RemoveVehicle(vehicleId);

            return true;
        }

        public async Task<bool> AddVehicleOnParking() {
            int option;
            int balance = 0;

            Console.WriteLine("Введіть ID ТЗ, яке хочете додати: ");

            string vehicleId = Console.ReadLine();

            if (!isIdValid(vehicleId)) {
                Console.WriteLine("Id повинно бути типу AA-1111-AA. Спробуйте ще раз!");

                return false;
            } else {
                Console.WriteLine("Оберіть тип ТЗ: \n 1 - PassengerCar \n 2 - Truck \n 3 -  Bus \n 4 - Motorcycle \n");

                try {

                    option = Int32.Parse(Console.ReadLine());
                } catch {

                    Console.WriteLine("Виберіть цифру та спробуйте ще раз! \n");
                    return false;
                }
                VehicleType vehicleType = VehicleType.PassengerCar;
                switch (option) {
                    case 1:
                        vehicleType = VehicleType.PassengerCar;
                        break;
                    case 2:
                        vehicleType = VehicleType.Truck;
                        break;
                    case 3:
                        vehicleType = VehicleType.Bus;
                        break;
                    case 4:
                        vehicleType = VehicleType.Motorcycle;
                        break;
                }
                Console.WriteLine("Введіть баланс: ");

                try {
                    balance = Int32.Parse(Console.ReadLine());

                    if (balance < 0) throw new Exception();
                } catch {

                    Console.WriteLine("Введіть невід'ємне число! \n");
                    return false;

                }

                try {
                    Vehicle vehicle = new Vehicle(vehicleId, vehicleType, balance);

                    var config = new MapperConfiguration(cfg => cfg.CreateMap<Vehicle, VehicleModel>());
                    var mapper = new Mapper(config);

                    VehicleModel model = mapper.Map<Vehicle, VehicleModel>(vehicle);
                    await _clientService.AddVehicle(model);
                } catch {

                    Console.WriteLine("Неможливо додати");
                    return false;
                }
                return true;

            }
        }

        public async Task ShowAllTransactionForPeriod() {
            var transactions = await _clientService.GetLastParkingTransactions();

            foreach (var transaction in transactions) {
                Console.WriteLine($"vehicle number: {transaction.VehicleId}\n" +
                    $"Sum: {transaction.Sum}\n" + $"Time: {transaction.TransactionDate}\n\n");
            }
        }

        public async Task ShowCurrentParkingBalance() {
            Console.WriteLine($"Поточний баланс паркінгу: {await _clientService.GetParkignBalance()} \n");
        }

        public async Task ShowEarnsMoneyForPeriod() {
            var transactionsSum = await _clientService.GetLastParkingTransactions();
            var sum = transactionsSum.Select(t => t.Sum).Sum();

            Console.WriteLine($"Сума зароблених коштів за поточний період: {transactionsSum} \n");
        }

        public async Task ShowFreePlaces() {
            Console.WriteLine($"Кількість вільних місць в паркінгу: {await _clientService.GetParkingFreePlaces()} \n");
        }

        public async Task ShowTransactionHistory() {
            try {

                Console.WriteLine($"Транзакції: { await _clientService.GetAllTrancsations()} \n");
            } catch (InvalidOperationException e) {

                Console.WriteLine(e.Message);
            } catch (Exception e) {

                Console.WriteLine(e.Message);
            }
        }

        public async Task ShowVehicles() {
            Console.WriteLine("Транспортні засоби паркінгу \n");

            var vehicles = await _clientService.GetVehicles();

            foreach (var vehicle in vehicles) {
                Console.WriteLine($"vehicleId: {vehicle.Id} \n");
                Console.WriteLine($"Type: {vehicle.VehicleType} \n");
                Console.WriteLine($"Balance: {vehicle.Balance} \n\n");
            }
        }

        public async Task<bool> TopUpVehicleBalance() {
            Console.WriteLine("Введіть номер ТЗ: ");

            string vehicleId = Console.ReadLine();

            if (!isIdValid(vehicleId)) {
                Console.WriteLine("Id повинно бути типу AA-1111-AA. Спробуйте ще раз!");

                return false;
            } 

            var vehicle = await _clientService.GetVehicle(vehicleId);

            if (vehicle == null) {
                Console.WriteLine("Даний ТЗ не існує");

                return false;
            }
            Console.WriteLine("Введіть суму на яку хочете поповнити баланс: ");
            string sum = Console.ReadLine();
            try {
                var intSum = Int32.Parse(sum);
                if (intSum > 0) {
                    var model = new TopUpVehicleModel();

                    model.Id = vehicleId;
                    model.Sum = intSum;

                    await _clientService.TopUpVehicle(model);

                    return true;
                } else {
                    Console.WriteLine("Сумма повинна бути більша 0");

                    return false;
                }
            } catch {

                Console.WriteLine("Вводити можна тільки числа.");
                return false;
            }
        }
        public bool isIdValid(string vehicleId) {
            string pattern = "[A-Z][A-Z][-][0-9]{4}[-][A-Z][A-Z]";
            return (Regex.IsMatch(vehicleId, pattern));
        }
    }
}
