﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Interfaces
{
    interface IConsoleService
    {
        void ShowCurrentParkingBalance();
        void ShowEarnsMoneyForPeriod();
        void ShowFreePlaces();
        void ShowAllTransactionForPeriod();
        void ShowTransactionHistory();
        void ShowVehicles();
        bool AddVehicleOnParking();
        bool RemoveVehicleFromParking();
        bool TopUpVehicleBalance();
    }
}
