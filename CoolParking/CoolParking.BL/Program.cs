﻿using System;
using System.Collections.Generic;
using System.Text;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.BL.Interfaces;


namespace CoolParking.BL
{
    public class Program {
        public static void Main(string[] args) {
            string option;
            int parseOption = 0;
            int maxOptionValue = 10;
            int minOptionValue = 1;
            LogService logService = new LogService(@"C:\Users\Denis\Documents\GitHub\coolparking\Transaction.log");
            TimerService withdrawTimer = new TimerService();
            TimerService logTimer = new TimerService();
            var parkingService = new ParkingService(withdrawTimer, logTimer, logService);
            while (true) {
                Console.WriteLine("Виберіть опцію, яку хочете використати: \n");
                Console.WriteLine("1: Вивести на екран поточний баланс Паркінгу \n");
                Console.WriteLine("2: Вивести на екран суму зароблених коштів за поточний період \n");
                Console.WriteLine("3: Вивести на екран кількість вільних місць на паркуванні \n");
                Console.WriteLine("4: Вивести на екран усі Транзакції Паркінгу за поточний період \n");
                Console.WriteLine("5: Вивести на екран історію Транзакцій \n");
                Console.WriteLine("6: Вивести на екран список Тр. засобів , що знаходяться на Паркінгу \n");
                Console.WriteLine("7: Поставити Транспортний засіб на Паркінг \n");
                Console.WriteLine("8: Забрати Транспортний засіб з Паркінгу \n");
                Console.WriteLine("9: Поповнити баланс конкретного Тр. засобу \n");
                Console.WriteLine("10: Вихід\n");
                Console.WriteLine("Ваш вибір: ");
                option = Console.ReadLine();
                try {
                    parseOption = Int32.Parse(option);
                } catch {
                    Console.WriteLine("Невірний вхідний формат! Спробуйте ще");
                    continue;
                }
                var console = new ConsoleService(parkingService);
                if (parseOption >= minOptionValue && parseOption <= maxOptionValue) {
                    switch (parseOption) {
                        case 1:
                            console.ShowCurrentParkingBalance();
                            break;
                        case 2:
                            console.ShowEarnsMoneyForPeriod();
                            break;
                        case 3:
                            console.ShowFreePlaces();
                            break;
                        case 4:
                            console.ShowAllTransactionForPeriod();
                            break;
                        case 5:
                            console.ShowTransactionHistory();
                            break;
                        case 6:
                            console.ShowVehicles();
                            break;
                        case 7:
                            console.AddVehicleOnParking();
                            break;
                        case 8:
                            console.RemoveVehicleFromParking();
                            break;
                        case 9:
                            console.TopUpVehicleBalance();
                            break;
                        case 10:
                            return;
                    }
                        
                } else {
                    Console.WriteLine("Значення повинні бути між 1 та 10");
                    continue;
                }
            }
           
        }
    }
}
