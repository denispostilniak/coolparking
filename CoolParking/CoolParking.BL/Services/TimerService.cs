﻿using System.Timers;
using CoolParking.BL.Interfaces;
namespace CoolParking.BL.Services {
    public class TimerService : ITimerService
    {
        public double Interval { get ; set; }
        readonly Timer timer = new Timer();

        public event ElapsedEventHandler Elapsed;

        public void Dispose() {
            timer.Dispose();
        }

        public void Start() {
            timer.Elapsed += Elapsed;
            timer.Interval = Interval;
            timer.AutoReset = true;
        }

        public void Stop() {
            timer.Elapsed -= Elapsed;
            timer.Stop();
        }
    }
}