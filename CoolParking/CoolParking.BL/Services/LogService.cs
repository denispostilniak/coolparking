﻿using System;
using System.IO;
using CoolParking.BL.Interfaces;
namespace CoolParking.BL.Services {
    public class LogService : ILogService
    {
        public LogService(string logFilePath) {
            LogPath = logFilePath;
        }

        public string LogPath { get; }

        public string Read() {
            string result = "";
            try {
                using StreamReader sr = new StreamReader(LogPath);
                result = sr.ReadToEnd();
            } catch (FileNotFoundException ex) {
                Console.WriteLine(ex.Message);
                throw new InvalidOperationException();
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
            return result;
        }

        public void Write(string logInfo) {
            try {
                if (!File.Exists(LogPath))
                    File.Create(LogPath).Close();

                using (StreamWriter sw = new StreamWriter(LogPath, true, System.Text.Encoding.Default)) {
                     sw.Write(logInfo + "\n");
                }
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }

        }
    }
}