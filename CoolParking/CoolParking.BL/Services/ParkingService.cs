﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
namespace CoolParking.BL.Services {
    public class ParkingService : IParkingService {
        readonly ILogService _logService;
        readonly ITimerService _logTimer;
        readonly ITimerService _withdrawTimer;
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService) {
            _logService = logService;
            _logTimer = logTimer;
            _logTimer.Interval = Settings.periodLogWrite * 1000;
            _logTimer.Elapsed += LoggingEventHandler;
            _withdrawTimer = withdrawTimer;
            _withdrawTimer.Interval = Settings.periodTransaction * 1000;
            _withdrawTimer.Elapsed += WithdrawEventHandler;
        }

        private void LoggingEventHandler(object sender, ElapsedEventArgs e) {
            string logger = "";

            foreach (var transaction in Parking.Transactions) {
                logger += $"vehicle number: {transaction.VehicleId}\n" + $"Sum: {transaction.Sum}\n" + $"Time: {transaction.Time}\n\n";
            }
            Parking.Transactions.Clear();
            _logService.Write(logger);
        }

        private void WithdrawEventHandler(object sender, ElapsedEventArgs e) {
            foreach (var vehicle in Parking.TransportVehicles) {
                decimal transaction = 0;

                switch (vehicle.VehicleType) {
                    case VehicleType.PassengerCar: {
                            if(vehicle.Balance - Settings.dependsOnVehicleType.GetValueOrDefault("Car") >= 0) {
                                transaction = Settings.dependsOnVehicleType.GetValueOrDefault("Car");
                            } else if(vehicle.Balance <= 0) {
                                transaction = Settings.dependsOnVehicleType.GetValueOrDefault("Car") * Settings.fineCoefficient;
                            } else {
                                transaction = vehicle.Balance + (Math.Abs(vehicle.Balance - Settings.dependsOnVehicleType.GetValueOrDefault("Car")) * Settings.fineCoefficient);
                            }
                              
                            break;
                        }
                    case VehicleType.Truck: {
                            if (vehicle.Balance - Settings.dependsOnVehicleType.GetValueOrDefault("Truck") >= 0) {
                                transaction = Settings.dependsOnVehicleType.GetValueOrDefault("Truck");
                            } else if (vehicle.Balance <= 0) {
                                transaction = Settings.dependsOnVehicleType.GetValueOrDefault("Truck") * Settings.fineCoefficient;
                            } else {
                                transaction = vehicle.Balance + (Math.Abs(vehicle.Balance - Settings.dependsOnVehicleType.GetValueOrDefault("Truck")) * Settings.fineCoefficient);
                            }
                           
                            break;
                        }
                    case VehicleType.Bus: {
                            if (vehicle.Balance - Settings.dependsOnVehicleType.GetValueOrDefault("Bus") >= 0) {
                                transaction = Settings.dependsOnVehicleType.GetValueOrDefault("Bus");
                            } else if (vehicle.Balance <= 0) {
                                transaction = Settings.dependsOnVehicleType.GetValueOrDefault("Bus") * Settings.fineCoefficient;
                            } else {
                                transaction = vehicle.Balance + (Math.Abs(vehicle.Balance - Settings.dependsOnVehicleType.GetValueOrDefault("Bus")) * Settings.fineCoefficient);
                            }
                           
                            break;
                        }
                    case VehicleType.Motorcycle: {
                            if (vehicle.Balance - Settings.dependsOnVehicleType.GetValueOrDefault("Motorbike") >= 0) {
                                transaction = Settings.dependsOnVehicleType.GetValueOrDefault("Motorbike");
                            } else if (vehicle.Balance <= 0) {
                                transaction = Settings.dependsOnVehicleType.GetValueOrDefault("Motorbike") * Settings.fineCoefficient;
                            } else {
                                transaction = vehicle.Balance + (Math.Abs(vehicle.Balance - Settings.dependsOnVehicleType.GetValueOrDefault("Motorbike")) * Settings.fineCoefficient);
                            }

                            break;
                        }
                }

                vehicle.Balance -= transaction;
                Parking.Balance += transaction;

                Parking.Transactions.Add(new TransactionInfo(vehicle.Id, transaction));
            }
        }

        public decimal GetBalance() {
            return Parking.Balance;
        }
        public int GetCapacity() {
            return Settings.parkingCapacity;
        }
        public int GetFreePlaces() {
            int availablePlaces = GetCapacity() - Parking.TransportVehicles.Count;
            return availablePlaces;
        }
        public ReadOnlyCollection<Vehicle> GetVehicles() {
            return new ReadOnlyCollection<Vehicle>(Parking.TransportVehicles);
        }
        public void AddVehicle(Vehicle vehicle) {
            if (Parking.TransportVehicles.Count == Settings.parkingCapacity) {
                throw new InvalidOperationException();
            } 
            if(!IsIdValid(vehicle.Id) || vehicle.Balance <= 0) {
                throw new ArgumentException("Incorrect parameters");
            }
            var searchClonevehicle = Parking.TransportVehicles.Find(c => c.Id == vehicle.Id);
            if (searchClonevehicle != null) {
               throw new ArgumentException("Existing vehicle");
            } 
            if(vehicle.VehicleType < 0 || vehicle.VehicleType > VehicleType.Motorcycle) {
                throw new ArgumentException();
            }
            Parking.TransportVehicles.Add(vehicle);
        }
        public void RemoveVehicle(string vehicleId) {
            if (!IsIdValid(vehicleId)) {
                throw new InvalidOperationException();
            }
           var preparedvehicle = Parking.TransportVehicles.Find(c => c.Id == vehicleId);

            if (preparedvehicle == null) {
                throw new ArgumentException();
            }

            if (preparedvehicle.Balance < 0) {
                throw new InvalidOperationException();
            } 

            Parking.TransportVehicles.Remove(preparedvehicle);
        }
        public void TopUpVehicle(string vehicleId, decimal sum) {
            if (!IsIdValid(vehicleId)) {
                throw new ArgumentException();
            }

            if (sum < 0) {
                throw new ArgumentException("Can't be negative sum");
            }

            var findedvehicle = Parking.TransportVehicles.Find(c => c.Id == vehicleId);

            if (findedvehicle == null)  {
                throw new InvalidOperationException("Can't find this vehicle");
            } 
            
            findedvehicle.Balance += sum;
        }
        public TransactionInfo[] GetLastParkingTransactions() {
            return Parking.Transactions.ToArray();
        }
        public string ReadFromLog() {
           return _logService.Read();
        }
        public void Dispose() {
            _withdrawTimer.Dispose();
            _logTimer.Dispose();

            Parking.Balance = 0;
            Parking.TransportVehicles = new List<Vehicle>(Settings.parkingCapacity);
            Parking.Transactions = new List<TransactionInfo>();
        }
        public bool IsIdValid(string id) {
            string regularExpression = "[A-Z][A-Z][-][0-9]{4}[-][A-Z][A-Z]";
            return (Regex.IsMatch(id, regularExpression));
        }
    }
}
