﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    class ConsoleService : IConsoleService
    {
        readonly ParkingService _parkingService;
        public ConsoleService(ParkingService parkingService) {
            _parkingService = parkingService;
        }

        public bool RemoveVehicleFromParking() {
            Console.WriteLine("Введіть ID ТЗ, якого хочете вилучити: ");
            string vehicleId=Console.ReadLine();
            if (!isIdValid(vehicleId)) {
                Console.WriteLine("Id повинно бути типу AA-1111-AA. Спробуйте ще раз!");
                return false;
            } else {
                var vehicle = _parkingService.GetVehicles().FirstOrDefault(v => v.Id == vehicleId);
                if (vehicle == null) {
                    Console.WriteLine("Даний ТЗ не існує");
                    return false;
                }
                _parkingService.RemoveVehicle(vehicleId);
                return true;
            }
        }

        public bool AddVehicleOnParking() {
            int option;
            int balance = 0;
            Console.WriteLine("Введіть ID ТЗ, яке хочете додати: ");
            string vehicleId = Console.ReadLine();
            if (!isIdValid(vehicleId)) {
                Console.WriteLine("Id повинно бути типу AA-1111-AA. Спробуйте ще раз!");
                return false;
            } else {
                Console.WriteLine("Оберіть тип ТЗ: \n 1 - PassengerCar \n 2 - Truck \n 3 -  Bus \n 4 - Motorcycle \n");
                try {
                     option = Int32.Parse(Console.ReadLine());
                } catch {
                    Console.WriteLine("Виберіть цифру та спробуйте ще раз! \n");
                    return false;
                }
                VehicleType vehicleType = VehicleType.PassengerCar;
                switch (option) {
                    case 1:
                        vehicleType = VehicleType.PassengerCar;
                        break;
                    case 2:
                        vehicleType = VehicleType.Truck;
                        break;
                    case 3:
                        vehicleType = VehicleType.Bus;
                        break;
                    case 4:
                        vehicleType = VehicleType.Motorcycle;
                        break;
                }
                Console.WriteLine("Введіть баланс: ");
                try {
                    balance = Int32.Parse(Console.ReadLine());
                    if (balance < 0) throw new Exception();
                } catch {
                    Console.WriteLine("Введіть невід'ємне число! \n");
                    return false;

                }
                try {
                    Vehicle vehicle = new Vehicle(vehicleId, vehicleType, balance);
                    _parkingService.AddVehicle(vehicle);
                } catch {
                    Console.WriteLine("Неможливо додати");
                    return false;
                }
                return true;

            }
        }

        public void ShowAllTransactionForPeriod() {
            var transactions = _parkingService.GetLastParkingTransactions();
            foreach( var transaction in transactions) {
                Console.WriteLine( $"vehicle number: {transaction.VehicleId}\n" + 
                    $"Sum: {transaction.Sum}\n" + $"Time: {transaction.Time}\n\n");
            }
        }

        public void ShowCurrentParkingBalance() {
            Console.WriteLine($"Поточний баланс паркінгу: {Parking.Balance} \n");
        }

        public void ShowEarnsMoneyForPeriod() {
            var transactionsSum = _parkingService.GetLastParkingTransactions().Select(t => t.Sum).Sum();
            Console.WriteLine($"Сума зароблених коштів за поточний період: {transactionsSum} \n");
        }

        public void ShowFreePlaces() {
            Console.WriteLine($"Кількість вільних місць в паркінгу: {_parkingService.GetFreePlaces()} \n");
        }

        public void ShowTransactionHistory() {
            try {
                Console.WriteLine($"Транзакції: {_parkingService.ReadFromLog()} \n");
            } catch(InvalidOperationException e) {
                Console.WriteLine(e.Message);
            } catch(Exception e) {
                Console.WriteLine(e.Message);
            }
        }

        public void ShowVehicles() {
            Console.WriteLine("Транспортні засоби паркінгу \n");
            var vehicles = Parking.TransportVehicles;
            foreach(var vehicle in vehicles) {
                Console.WriteLine($"vehicleId: {vehicle.Id} \n");
                Console.WriteLine($"Type: {vehicle.VehicleType} \n");
                Console.WriteLine($"Balance: {vehicle.Balance} \n\n");
            }
        }

        public bool TopUpVehicleBalance() {
            Console.WriteLine("Введіть номер ТЗ: ");
            string vehicleId = Console.ReadLine();
            if (!isIdValid(vehicleId)) {
                Console.WriteLine("Id повинно бути типу AA-1111-AA. Спробуйте ще раз!");
                return false;
            } else {
                var vehicle = _parkingService.GetVehicles().FirstOrDefault(v => v.Id == vehicleId);
                if (vehicle == null) {
                    Console.WriteLine("Даний ТЗ не існує");
                    return false;
                }
                Console.WriteLine("Введіть суму на яку хочете поповнити баланс: ");
                string sum = Console.ReadLine();
                try {
                    var intSum = Int32.Parse(sum);
                    if (intSum > 0) {
                        _parkingService.TopUpVehicle(vehicleId, intSum);
                        return true;
                    } else {
                        Console.WriteLine("Сумма повинна бути більша 0");
                        return false;
                    }
                } catch {
                    Console.WriteLine("Вводити можна тільки числа.");
                    return false;
                }
            }
        }
        public bool isIdValid(string vehicleId) {
            string pattern = "[A-Z][A-Z][-][0-9]{4}[-][A-Z][A-Z]";
            return (Regex.IsMatch(vehicleId, pattern));
        }
    }
}
