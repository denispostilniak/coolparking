﻿using System;
namespace CoolParking.BL.Models {
    public struct TransactionInfo {
        public string VehicleId { get; private set; }
        public decimal Sum { get; private set; }
        public DateTime Time { get; private set; }
        public TransactionInfo(string _VehicleId, decimal _Sum) {
            VehicleId = _VehicleId;
            Sum = _Sum;
            Time = DateTime.Now;
        }
    }
}   