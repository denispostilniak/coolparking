﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models {
    public class Vehicle {
        public Vehicle(string _id, VehicleType _vehicleType, decimal _balance) {
            if(Parking.TransportVehicles.Count == Settings.parkingCapacity) {
                throw new InvalidOperationException();
            }
            string regularExpression = "[A-Z][A-Z][-][0-9]{4}[-][A-Z][A-Z]";
            if (Regex.IsMatch(_id, regularExpression)) {
                Id = _id;
            } else throw new ArgumentException("Wrong ID");
            VehicleType = _vehicleType;
            if (_balance >= 0) {
                Balance = _balance;
            } else throw new ArgumentException("Can't be negative balance");
        }

        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; set; }
        public static string GenerateRandomRegistrationPlateNumber() {
            string X1 = "", X2 = "", X3 = "", X4 = "", Y1 = "", Y2 = "", Y3 = "", Y4 = "";
            try {
                List<string> lettersList = new List<string>() { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
                Random random = new Random();
                 X1 = random.Next(9).ToString();
                 X2 = random.Next(9).ToString();
                 X3 = random.Next(9).ToString();
                 X4 = random.Next(9).ToString();
                 Y1 = lettersList[random.Next(lettersList.Count) - 1];
                 Y2 = lettersList[random.Next(lettersList.Count) - 1];
                 Y3 = lettersList[random.Next(lettersList.Count) - 1];
                 Y4 = lettersList[random.Next(lettersList.Count) - 1];  
            } catch {
                throw new Exception();
            }
            return $"{Y1} + {Y2} + '-' + {X1} + {X2} + {X3} + {X4} + '-' + {Y3} + {Y4}";
        }
    }
}