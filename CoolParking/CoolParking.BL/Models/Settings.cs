﻿using System.Collections.Generic;

namespace CoolParking.BL.Models {
    public static class Settings {
        public static readonly decimal initialBalance = 0;
        public static readonly int parkingCapacity = 10;
        public static readonly double periodTransaction = 5;
        public static readonly double periodLogWrite = 60;
        public static readonly decimal fineCoefficient = 2.5M;
        public static Dictionary<string, decimal> dependsOnVehicleType = new Dictionary<string, decimal>() {
            { "Car", 2M},
            { "Truck", 5M},
            { "Bus", 3.5M},
            { "Motorbike", 1M}
        };
    }
}