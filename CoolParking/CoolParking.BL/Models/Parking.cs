﻿using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models {
    public class Parking {
        private static Parking parkingInstance;
        private Parking() { }

        public static Parking getParkingInstance() {
            if (parkingInstance == null) {
                parkingInstance = new Parking();
            }
            return parkingInstance;
        }
        public static decimal Balance = Settings.initialBalance;
        public static List<Vehicle> TransportVehicles = new List<Vehicle>(Settings.parkingCapacity);
        public static List<TransactionInfo> Transactions = new List<TransactionInfo>();
    }
}